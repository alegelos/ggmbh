//
//  TrackMediaCell.swift
//  gGmbH
//
//  Created by Alejandro Gelos on 12/4/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import UIKit

final class TrackMediaCell: UITableViewCell {
    
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var thierTitle: UILabel!
    
    private var coverImageUrl: String?
    
    func loadCell(withTrack track: Track, mediaManager: MediaServicesManagerProtocol) {
        loadFirstLabel (withTrack: track)
        loadSecondLabel(withTrack: track)
        loadThierdLabel(withTrack: track)
        
        coverImage.image = nil
        coverImageUrl    = nil
        if let coverImageUrl = track.artworkUrl60{
            self.coverImageUrl = coverImageUrl
            loadCoverImage(withMediaManager: mediaManager)
        }
    }
}


// MARK: - Private Methods
extension TrackMediaCell {
    
    private func loadFirstLabel(withTrack track: Track) {
        title.text = track.trackName
    }
    
    private func loadSecondLabel(withTrack track: Track) {
        subTitle.text = ""
        if let artistName = track.artistName {
            subTitle.text = artistName
        }
        if let collectionName = track.collectionName {
            subTitle.text = subTitle.text! + " - " + collectionName
        }
    }
    
    private func loadThierdLabel(withTrack track: Track) {
        thierTitle.text = ""
        if let trackTime = track.trackTime {
            thierTitle.text = String(trackTime)
        }
        if let genre = track.primaryGenreName {
            thierTitle.text = thierTitle.text! + "  -  " + genre
        }
        if let year = track.releaseYear {
            thierTitle.text = thierTitle.text! + "  -  " + year
        }
        if let price = track.trackPrice {
            thierTitle.text = thierTitle.text! + "  --  " + String(price) + "€"
        }
    }

    private func loadCoverImage(withMediaManager mediaManager: MediaServicesManagerProtocol) {
        guard let coverImageUrl = coverImageUrl else{return}
        DispatchQueue.main.async { [weak self] in
            self?.loadingIndicator.startAnimating()
        }
        
        mediaManager.getImage(fromUrl: coverImageUrl) { [weak self] result in
            guard let `self` = self else{return}
            
            switch result {
            case .failure(let error):
                print(error)
            case .success(let image, let url):
                guard mediaManager.isResponseValid(askedUrl: self.coverImageUrl, imageUrl: url) else {return}//Discard images from old request
                DispatchQueue.main.async {
                    self.loadingIndicator.stopAnimating()
                    self.coverImage.image = image
                }
            }
        }
    }
}
