//
//  MediaPlayerCollectionCell.swift
//  gGmbH
//
//  Created by Alejandro Gelos on 12/7/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import UIKit

final class MediaPlayerCollectionCell: UICollectionViewCell {
    @IBOutlet weak var trackCoverImage: UIImageView!
    @IBOutlet weak var trackTitle: UILabel!
    @IBOutlet weak var trackArtist: UILabel!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    private var coverImageUrl: String?
    
    func loadCell(withTrack track: Track, mediaManager: MediaServicesManagerProtocol) {
        loadFirstLabel (withTrack: track)
        loadSecondLabel(withTrack: track)
        
        if let coverImageUrl = track.artworkUrl100{
            trackCoverImage.image = nil
            self.coverImageUrl = coverImageUrl
            loadCoverImage(withMediaManager: mediaManager)
        }
    }
}

// MARK - Private Methods
extension MediaPlayerCollectionCell{
    
    private func loadFirstLabel(withTrack track: Track) {
        trackTitle.text = track.trackName
    }
    
    private func loadSecondLabel(withTrack track: Track) {
        trackArtist.text = track.artistName
    }
    
    private func loadCoverImage(withMediaManager mediaManager: MediaServicesManagerProtocol) {
        guard let coverImageUrl = coverImageUrl else {return}
        DispatchQueue.main.async { [weak self] in
            self?.loadingIndicator.startAnimating()
        }
        
        mediaManager.getImage(fromUrl: coverImageUrl) { [weak self] result in
            guard let `self` = self else {return}
            
            switch result {
            case .failure(let error):
                print(error)
            case .success(let image, let url):
                guard mediaManager.isResponseValid(askedUrl: self.coverImageUrl, imageUrl: url) else {return}//Discard images from old request
                DispatchQueue.main.async {
                    self.loadingIndicator.stopAnimating()
                    self.trackCoverImage.image = image
                }
            }
        }
    }
}
