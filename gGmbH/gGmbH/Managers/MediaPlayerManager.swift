//
//  MediaPlayerUtils.swift
//  gGmbH
//
//  Created by Alejandro Gelos on 12/7/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import AVFoundation

protocol MediaPlayerManagerDelegate: class {
    func didFinishPlaying()
    func nowPlaying(media:Int)
}

protocol MediaPlayer: class {
    func pauseMedia()
    func play()
    func playMedia(item: Int)
    func set(mediaList: [Track])
    func set(delegate: MediaPlayerManagerDelegate?)
    func getMediaList() -> [Track]
    func getCurrentTrack() -> Int
    var isPlayingMedia:AVPlayer.TimeControlStatus? {get}
}

final class MediaPlayerManager {
    weak var delegate: MediaPlayerManagerDelegate?
    
    static let shared = MediaPlayerManager()
    private init() {
        NotificationCenter.default.addObserver(self, selector: #selector(mediaTrackDidEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
    }
    private lazy var queue: OperationQueue = {
        var queue = OperationQueue()
        queue.qualityOfService = .utility
        queue.name = "Media player queue"
        return queue
    }()
    
    private var player: AVPlayer?
    private var mediaList = [Track]()
    private var currentTrack = -1
}

// MARK: - MediaPlayer
extension MediaPlayerManager:MediaPlayer {
    func set(delegate: MediaPlayerManagerDelegate?) {
        self.delegate = delegate
    }
    
    func getCurrentTrack() -> Int {
        return currentTrack
    }
    
    func getMediaList() -> [Track] {
        return mediaList
    }
    
    func playMedia(item: Int) {
        guard item >= 0, item < mediaList.count else {
            fatalError(gGmbHErrors.trackOutOfMediaListBounds.localizedDescription)
        }
        playMedia(fromMediaUrl: mediaList[item].previewUrl)
        currentTrack = item
        delegate?.nowPlaying(media: currentTrack)
    }
    
    var isPlayingMedia:AVPlayer.TimeControlStatus? {
        return player?.timeControlStatus
    }
    
    func set(mediaList: [Track]) {
        self.mediaList = mediaList
    }
    
    func pauseMedia() {
        queue.addOperation { [unowned self] in
            self.player?.pause()
        }
    }
    
    func play() {
        queue.addOperation { [unowned self] in
            self.player?.play()
        }
    }
}

// MARK: - Private methods
extension MediaPlayerManager {
    @objc private func mediaTrackDidEnd() {
        guard currentTrack + 1 < mediaList.count else {
            currentTrack = -1
            delegate?.didFinishPlaying()
            return
        }
        playMedia(item: currentTrack + 1)
    }
    
    private func playMedia(fromMediaUrl mediaUrl:String?) {
        queue.addOperation { [unowned self] in
            guard let urlString =  mediaUrl, let url = URL(string: urlString) else {
                print(gGmbHErrors.failtToDownloadSampleTrack.localizedDescription)
                return
            }
            
            let playerItem: AVPlayerItem = AVPlayerItem(url: url)
            self.player = AVPlayer(playerItem: playerItem)
            self.player?.play()
        }
    }
}
