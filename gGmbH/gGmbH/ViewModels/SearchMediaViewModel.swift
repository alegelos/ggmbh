//
//  SearchMediaViewModel.swift
//  gGmbH
//
//  Created by Alejandro Gelos on 12/3/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import UIKit

final class SearchMediaViewModel {
    
    var results:  Box<[Track]> = Box([Track]())
    var sortType: Box<Sorts>   = Box(Sorts.Genre)
    var selectedItem: Int?
    private var searchText = ""{
        didSet {
            guard !searchText.isEmpty else {results.value.removeAll();return}
            search(text: searchText)
        }
    }
    
    private let sorter: SorterUtils!
    private let iTunesUtils:   ITunesUtils!
    private let iTunesManager: ITunesServicesManagerProtocol!
    
    init(iTunesManager: ITunesServicesManagerProtocol = ITunesServicesManager(), itunesUtils: ITunesUtils = ITunesUtils(), withSorter sorter: SorterUtils = SorterUtils()) {
        self.iTunesManager = iTunesManager
        self.iTunesUtils   = itunesUtils
        self.sorter = sorter
    }
    
    func update(searchText text: String) {
        searchText = text
    }
    
    func sortButtonPressed() {
        sortType.value = sortType.value.getNextSortType()
        results.value = sorter.sort(tracks: results.value, by: sortType.value)
    }
    
    func didReceiveMemoryWarning() {
        iTunesManager.cleanCache()
    }
    
    func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.destination {
        case let mediaPlayer as MediaPlayerViewController:
            mediaPlayer.mediaPlayerViewModel.results.value = results.value
            if let selectedItem = selectedItem {
                mediaPlayer.mediaPlayerViewModel.currentTrack.value = selectedItem
            }
        default:
            break
        }
    }
}


// MARK: - Private methods
extension SearchMediaViewModel {
    private func search(text: String) {
        iTunesManager.search(forText: text, completion: {[weak self] result in
            guard let `self` = self else {return}
            
            switch result {
            case .failure(let error):
                print(error)
                self.results.value.removeAll()
            case .success(let mediaResults, let response):
                guard self.iTunesUtils.isSearchResponseValid(searchText: self.searchText, response: response) else {return}
                self.results.value = self.sorter.sort(tracks: mediaResults, by: self.sortType.value)
            }
        })
    }
}
