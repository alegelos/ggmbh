//
//  TrackCoverCollectionViewHandler.swift
//  gGmbH
//
//  Created by Alejandro Gelos on 12/7/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import UIKit

protocol TrackCoverCollectionViewHandlerDelegate: class {
    func reloadPages()
    func didScroll(toItem item: Int)
    func beginScrolling()
}

final class TrackCoverCollectionViewHandler: NSObject {
    weak var delegate: TrackCoverCollectionViewHandlerDelegate?
    
    var allMedia:[Track] = []{
        didSet{
            delegate?.reloadPages()
        }
    }
    
    private let mediaManager:MediaServicesManagerProtocol!
    
    init(mediaManager: MediaServicesManagerProtocol = MediaServicesManager()) {
        self.mediaManager = mediaManager
    }
    
    func didReceiveMemoryWarning() {
        mediaManager.cleanCache()
    }
}

// MARK: - UICollectionViewDataSource
extension TrackCoverCollectionViewHandler: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allMedia.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellsType.MediaPlayerCollectionCell.rawValue, for: indexPath) as? MediaPlayerCollectionCell else {
            fatalError(gGmbHErrors.failtToDequeCell.localizedDescription)
        }
        cell.loadCell(withTrack: allMedia[indexPath.row], mediaManager: mediaManager)
        return cell
    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        delegate?.beginScrolling()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        delegate?.didScroll(toItem: getScrolledItem(scrollView: scrollView))
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension TrackCoverCollectionViewHandler: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height)
    }
}

// MARK: - PRivate methods
extension TrackCoverCollectionViewHandler {
    private func getScrolledItem(scrollView: UIScrollView) -> Int {
        let scrolledPoint = Int(scrollView.contentOffset.x)
        guard scrolledPoint < Int(scrollView.contentSize.width) else {
            return allMedia.count - 1
        }
        guard scrolledPoint > 0 else {
            return 0
        }
        let item = (scrolledPoint / (Int(scrollView.contentSize.width) / allMedia.count))
        return item
    }
}
