//
//  SearchMediaViewController.swift
//  gGmbH
//
//  Created by Alejandro Gelos on 12/3/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import UIKit

final class SearchMediaViewController: UIViewController {
    @IBOutlet weak var searchTableView: UITableView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var sortButton: UIButton!
    @IBOutlet weak var sortViewHeight: NSLayoutConstraint!
    
    
    private let tableViewHandler = SearchMediaTableViewHandler()
    private let searchController = UISearchController(searchResultsController: nil)
    private let viewModel = SearchMediaViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavBar()
        setUpSearchBar()
        setUpTableView()
        setUpViewController()
        setUpBinding()
    }
    
    @IBAction func didPressSortButton(_ sender: Any) {
        viewModel.sortButtonPressed()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        viewModel.prepare(for: segue, sender: sender)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        viewModel.didReceiveMemoryWarning()
    }
}

// MARK: - SearchBar delegates
extension SearchMediaViewController: UISearchResultsUpdating{
    func updateSearchResults(for searchController: UISearchController) {
        guard let text = searchController.searchBar.text else {return}
        viewModel.update(searchText: text)
        updateSortView(withText: text)
        DispatchQueue.main.async { [weak self] in
            self?.loadingIndicator.startAnimating()
        }
    }
}

// MARK: - SearchMediaTableViewHandlerDelegate
extension SearchMediaViewController:SearchMediaTableViewHandlerDelegate{
    func didSelect(item: Int) {
        viewModel.selectedItem = item
        performSegue(withIdentifier: GGMBHSegues.GoToMediaPlayer.rawValue, sender: self)
    }
    
    func reloadTable() {
        DispatchQueue.main.async {[weak self] in
            self?.searchTableView.reloadData()
        }
    }
}

// MARK: - Private methods
extension SearchMediaViewController{
    private func setUpSearchBar() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        definesPresentationContext = true
    }
    
    private func setUpTableView(){
        searchTableView.delegate  = tableViewHandler
        searchTableView.dataSource = tableViewHandler
        searchTableView.tableFooterView = UIView()
        tableViewHandler.delegate = self
    }
    
    private func setUpNavBar(){
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    private func setUpViewController() {
        title = "Search Media"
    }
    
    private func setUpBinding() {
        viewModel.results.bind { [weak self] in
            self?.tableViewHandler.allMedia = $0
            DispatchQueue.main.async {
                self?.loadingIndicator.stopAnimating()
            }
        }
        
        viewModel.sortType.bind { [weak self] sortType in
            DispatchQueue.main.async {
                self?.sortButton.setImage(UIImage(named: sortType.rawValue), for: .highlighted)
                self?.sortButton.setImage(UIImage(named: sortType.rawValue), for: .normal)
            }
        }
    }
    
    private func updateSortView(withText text: String) {
        if text.isEmpty {
            if !isSortViewHidden() {
                setSortView(hidden: true)
            }
        } else if isSortViewHidden() {
            setSortView(hidden: false)
        }
    }
    
    private func setSortView(hidden: Bool) {
        sortViewHeight.constant = hidden ? 0 : 35
        let time = hidden ? 0 : 0.3
        UIView.animate(withDuration: time, animations: { [weak self] in
            self?.view.layoutIfNeeded()
        })
    }
    
    private func isSortViewHidden() -> Bool {
        return sortViewHeight.constant == 0
    }
}
