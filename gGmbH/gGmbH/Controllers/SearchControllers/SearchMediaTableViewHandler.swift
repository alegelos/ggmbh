//
//  SearchMediaTableViewHandler.swift
//  gGmbH
//
//  Created by Alejandro Gelos on 12/4/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import UIKit

protocol SearchMediaTableViewHandlerDelegate: class {
    func didSelect(item: Int)
    func reloadTable()
}

final class SearchMediaTableViewHandler:NSObject {
    weak var delegate: SearchMediaTableViewHandlerDelegate?

    private let mediaMAnager: MediaServicesManager!
    
    init(mediaManager: MediaServicesManager = MediaServicesManager()) {
        self.mediaMAnager = mediaManager
    }
    
    var allMedia:[Track] = [] {
        didSet{
           delegate?.reloadTable()
        }
    }
}

// MARK: - UITableViewDelegate
extension SearchMediaTableViewHandler: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.didSelect(item: indexPath.row)
    }
}

// MARK: - UITableViewDataSource
extension SearchMediaTableViewHandler: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allMedia.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellsType.SearchTrackTableViewCell.rawValue, for: indexPath) as? TrackMediaCell else {
            fatalError(gGmbHErrors.failtToDequeCell.localizedDescription)
        }
        cell.loadCell(withTrack: allMedia[indexPath.row], mediaManager: mediaMAnager)
        return cell
    }
}
