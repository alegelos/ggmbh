//
//  MediaPlayerViewController.swift
//  gGmbH
//
//  Created by Alejandro Gelos on 12/7/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import UIKit

final class MediaPlayerViewController: UIViewController {

    @IBOutlet weak var trackCoverCollectionView: UICollectionView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    
    let collectionViewHandler = TrackCoverCollectionViewHandler()
    let mediaPlayerViewModel  = MediaPlayerViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpCollectionView()
        setUpBinding()
        mediaPlayerViewModel.controllerDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(willResignActiveNotification), name: UIApplication.willResignActiveNotification, object: nil)
    }
    
    @IBAction func playButtonPressed(_ sender: Any) {
        mediaPlayerViewModel.playButtonPressed()
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        mediaPlayerViewModel.nextButtonPressed()
    }
    
    @IBAction func previousButtonPressed(_ sender: Any) {
        mediaPlayerViewModel.previousButtonPressed()
    }
    
    @IBAction func shareButtonPressed(_ sender: Any) {
        mediaPlayerViewModel.shareButtonPressed()
    }
    
    @IBAction func closeButtonPressed(_ sender: Any) {
        mediaPlayerViewModel.closeButtonPressed()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        mediaPlayerViewModel.didReceiveMemoryWarning()
        collectionViewHandler.didReceiveMemoryWarning()
    }
}


// MARK: - TrackCoverCollectionViewHandlerDelegate
extension MediaPlayerViewController: TrackCoverCollectionViewHandlerDelegate {
    func beginScrolling() {
        disableButtons()
    }

    func didScroll(toItem item: Int) {
        mediaPlayerViewModel.didScroll(toItem: item)
    }

    func reloadPages() {
        DispatchQueue.main.async { [weak self] in
            self?.trackCoverCollectionView.reloadData()
            self?.mediaPlayerViewModel.collectionDidReload()
        }
    }
}

// MARK: - Private methods
extension MediaPlayerViewController {
    private func setUpCollectionView() {
        collectionViewHandler.delegate = self
        trackCoverCollectionView.delegate = collectionViewHandler
        trackCoverCollectionView.dataSource = collectionViewHandler
    }

    private func updateNextPreviousPlayButtonsState() {
        DispatchQueue.main.async { [weak self] in
            guard let `self` = self else {return}
            self.previousButton.isEnabled = self.mediaPlayerViewModel.currentTrack.value > 0
            self.nextButton.isEnabled = self.mediaPlayerViewModel.currentTrack.value < self.collectionViewHandler.allMedia.count - 1
            self.playButton.isEnabled = true
        }
    }

    private func scrollTo(currentTrack track: Int) {
        self.trackCoverCollectionView.scrollToItem(at: IndexPath(row: track, section: 0), at: [], animated: true)
    }

    private func setUpBinding() {
        mediaPlayerViewModel.results.bind { [unowned self] in
            self.collectionViewHandler.allMedia = $0
        }

        mediaPlayerViewModel.isPlayingTrack.bind { [weak self] in
            let image = $0 ? #imageLiteral(resourceName: "PauseIcon") : #imageLiteral(resourceName: "PlayIcon")
            DispatchQueue.main.async {
                self?.playButton.setImage(image, for: .normal)
                self?.playButton.setImage(image, for: .highlighted)
            }
        }

        mediaPlayerViewModel.currentTrack.bind { [weak self] currentTrack in
            DispatchQueue.main.async {
                self?.trackCoverCollectionView.layoutIfNeeded()
                self?.scrollTo(currentTrack: currentTrack)
                self?.updateNextPreviousPlayButtonsState()
            }
        }

        mediaPlayerViewModel.previewToShare.bind { [unowned self] in
            guard let link = $0 else {return}
            self.share(link)
        }

        mediaPlayerViewModel.closeAction.bind { [weak self] in
            guard $0 else {return}
            DispatchQueue.main.async {
                self?.dismiss(animated: true, completion: nil)
            }
        }
    }

    private func disableButtons() {
        DispatchQueue.main.async { [weak self] in
            self?.nextButton.isEnabled = false
            self?.playButton.isEnabled = false
            self?.previousButton.isEnabled = false
        }
    }
    
    @objc func willResignActiveNotification() {
        mediaPlayerViewModel.willResignActiveNotification()
    }
}
