//
//  SorterUtils.swift
//  gGmbH
//
//  Created by Alejandro Gelos on 12/7/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import Foundation

/**
 Sort an array of Tracks by the sort type
 - Parameters:
 - tracks: tracks to sort
 - sortBy: srot type to sort the array
 
 - Returns: an Array of tracks sorted by sort type.
 */
struct SorterUtils {
    func sort(tracks: [Track], by sortBy: Sorts) -> [Track] {
        var sortedTracks = tracks
        sortedTracks.sort { (a, b) in
            switch sortBy {
            case .Duration:
                guard let aTrackTimeMillis = a.trackTimeMillis else {return false}
                guard let bTrackTimeMillis = b.trackTimeMillis else {return false}
                return aTrackTimeMillis > bTrackTimeMillis
            case .Genre:
                guard let aGenre = a.primaryGenreName else {return false}
                guard let bGenre = b.primaryGenreName else {return false}
                return aGenre < bGenre
            case .Price:
                guard let aTrackPrice = a.trackPrice else {return false}
                guard let bTrackPrice = b.trackPrice else {return false}
                return aTrackPrice > bTrackPrice
            }
        }
        return sortedTracks
    }
}
