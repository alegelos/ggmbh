//
//  PListUtil.swift
//  gGmbH
//
//  Created by Alejandro Gelos on 12/3/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import Foundation

enum PListName:String {
    case none
}

struct PListUtils {
    func getDic(fromPList pList: PListName) -> Dictionary<String, Any>? {
        guard let dicPath = Bundle.main.path(forResource: pList.rawValue, ofType: "plist") else {return nil}
        return NSDictionary(contentsOfFile: dicPath) as? [String:Any]
    }
}
