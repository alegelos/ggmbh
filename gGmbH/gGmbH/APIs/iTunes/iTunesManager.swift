//
//  iTuneskManager.swift
//  NetworkLayer
//
//  Created by Alejandro Gelos on 12/2/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import Foundation

protocol ITunesServicesManagerProtocol {
    func cleanCache()
    func search(forText text: String, completion: @escaping (Result<[Track], URLResponse, ErrorResult>) -> Void)
}

struct ITunesServicesManager {
    static let environment: ITunesNetworkEnvironment = .production
    let router: Router<ITunesApi>!
    let iTunesUtils: ITunesUtils!
    private let searchCache = NSCache<NSString, StructWrapper<([Track], URLResponse)>>()
    
    init(_ router: Router<ITunesApi> = Router<ITunesApi>(),_ iTunesUtils:ITunesUtils = ITunesUtils() ) {
        self.router = router
        self.iTunesUtils = iTunesUtils
    }
}

// MARK: - ITunesManagerProtocol
extension ITunesServicesManager: ITunesServicesManagerProtocol {
    func cleanCache() {
        searchCache.removeAllObjects()
    }
    
    func search(forText text: String, completion: @escaping (Result<[Track], URLResponse, ErrorResult>) -> Void) {
        let searchText = iTunesUtils.prepareTextForRequest(text: text)

        //Search in cache first
        if let result = searchIncache(text: searchText) {
            completion(.success(result.value.0, result.value.1))
            return
        }
        
        //Do request
        router.request(withRoute: .search(for: searchText)) {data, response, error in
            guard error == nil else {
                completion(.failure(.network(string: error.debugDescription)))
                return
            }
            
            let result = NetworkResponseUtils().handleNetworkResponse(data: data, response: response)
            switch result {
            case .success(let data, let response):
                do {
                    let apiResponse = try JSONDecoder().decode(ITunesApiResponse.self, from: data)
                    self.searchCache.setObject(StructWrapper((apiResponse.results, response)), forKey: searchText as NSString)
                    completion(.success(apiResponse.results, response))
                } catch {
                    completion(.failure(.network(string: NetworkResponse.unableToDecode.rawValue)))
                }
            case .failure(let networkFailureError):
                completion(.failure(networkFailureError))
            }
        }
    }
}

// MARK: - Private methods
extension ITunesServicesManager {
    private func searchIncache(text: String) -> StructWrapper<([Track], URLResponse)>? {
        return searchCache.object(forKey: text as NSString)
    }
}
