//
//  ModelCodable.swift
//  NetworkLayer
//
//  Created by Alejandro Gelos on 12/2/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import Foundation

extension ITunesApiResponse: Decodable {
    
    private enum ITunesApiResponseCodingKeys: String, CodingKey {
        case resultCount
        case results
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ITunesApiResponseCodingKeys.self)
        do {
            resultCount = try container.decode(Int.self, forKey: .resultCount)
        } catch {resultCount = 0}
        do {
            results = try container.decode([Track].self, forKey: .results)
        } catch {results = [Track]()}
    }
}

struct ITunesApiResponse {
    let resultCount: Int
    let results: [Track]
}

extension Track: Decodable {
    
    enum TrackCodingKeys: String, CodingKey {
        case trackId
        case trackTime
        case trackName
        case artistName
        case previewUrl
        case trackPrice
        case releaseDate
        case collectionId
        case artworkUrl30
        case artworkUrl60
        case artworkUrl100
        case collectionName
        case trackTimeMillis
        case primaryGenreName
        case trackCensoredName
        case collectionArtistName
        case collectionCensoredName
    }
    
    
    init(from decoder: Decoder) throws {
        let trackContainer = try decoder.container(keyedBy: TrackCodingKeys.self)
        
        trackId                = try? trackContainer.decode(Int.self,    forKey: .trackId)
        trackName              = try? trackContainer.decode(String.self, forKey: .trackName)
        trackPrice             = try? trackContainer.decode(Double.self, forKey: .trackPrice)
        previewUrl             = try? trackContainer.decode(String.self, forKey: .previewUrl)
        artistName             = try? trackContainer.decode(String.self, forKey: .artistName)
        collectionId           = try? trackContainer.decode(Int.self,    forKey: .collectionId)
        artworkUrl30           = try? trackContainer.decode(String.self, forKey: .artworkUrl30)
        artworkUrl60           = try? trackContainer.decode(String.self, forKey: .artworkUrl60)
        artworkUrl100          = try? trackContainer.decode(String.self, forKey: .artworkUrl100)
        collectionName         = try? trackContainer.decode(String.self, forKey: .collectionName)
        trackTimeMillis        = try? trackContainer.decode(Double.self, forKey: .trackTimeMillis)
        primaryGenreName       = try? trackContainer.decode(String.self, forKey: .primaryGenreName)
        trackCensoredName      = try? trackContainer.decode(String.self, forKey: .trackCensoredName)
        collectionArtistName   = try? trackContainer.decode(String.self, forKey: .collectionArtistName)
        collectionCensoredName = try? trackContainer.decode(String.self, forKey: .collectionCensoredName)
        
        if let trackTimeMillis = trackTimeMillis {
            trackTime = TimeUtils().getFormatedTime(fromMilli: trackTimeMillis)
        } else {
            trackTime = nil
        }
        
        if let dateString = try? trackContainer.decode(String.self, forKey: .releaseDate) {
            let date = DateUtils().getDate(fromString: dateString)
            releaseDate = date
            releaseYear = DateUtils().getYear(fromDate: date)
        } else {
            releaseYear = nil
            releaseDate = nil
        }
    }
}
