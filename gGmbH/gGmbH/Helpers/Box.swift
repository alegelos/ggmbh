//
//  Box.swift
//  gGmbH
//
//  Created by Alejandro Gelos on 12/2/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import Foundation

final class Box<T> {
    typealias Listener = (T) -> Void
    var listener: Listener?
    
    var value: T {
        didSet {
            listener?(value)
        }
    }
    
    init(_ value: T) {
        self.value = value
    }
    
    func bind(listener: Listener?) {
        self.listener = listener
        listener?(value)
    }
}
