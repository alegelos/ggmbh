//
//  NetworkService.swift
//  NetworkLayer
//
//  Created by Alejandro Gelos on 12/2/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import Foundation

protocol NetworkRouter: class {
    associatedtype EndPoint: EndPointType
    func request(withRoute route: EndPoint, completion: @escaping NetworkRouterCompletion)
    func cancel()
}

final class Router<EndPoint: EndPointType>: NetworkRouter {
    private var task: URLSessionTask?
    
    private lazy var queue: OperationQueue = {
        var queue = OperationQueue()
        queue.qualityOfService = .background
        queue.name = "Router queue"
        return queue
    }()
    
    func request(withRoute route: EndPoint, completion: @escaping NetworkRouterCompletion) {
        queue.addOperation { [weak self] in
            guard let `self` = self else {return}
            let session = URLSession.shared
            do {
                let request = try self.buildRequest(from: route)
                NetworkLogger.log(request: request)
                self.task = session.dataTask(with: request, completionHandler: { data, response, error in
                    NetworkingUtils.activeNetworkProcess -= 1
                    completion(data, response, error)
                })
            } catch {
                NetworkingUtils.activeNetworkProcess -= 1
                completion(nil, nil, error)
            }
            self.task?.resume()
            NetworkingUtils.activeNetworkProcess += 1
        }
    }
    
    func cancel() {
        queue.addOperation { [weak self] in
            self?.task?.cancel()
        }
    }
}


// MARK: - Private Methods
extension Router {
    private func buildRequest(from route: EndPoint) throws -> URLRequest {
        
        var request = URLRequest(url: route.baseURL.appendingPathComponent(route.path),
                                 cachePolicy: .reloadIgnoringLocalAndRemoteCacheData,
                                 timeoutInterval: 10.0)
        
        request.httpMethod = route.httpMethod.rawValue
        do {
            switch route.task {
            case .request:
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            case .requestParameters(let bodyParameters,
                                    let bodyEncoding,
                                    let urlParameters):
                
                try configureParameters(bodyParameters: bodyParameters,
                                             bodyEncoding: bodyEncoding,
                                             urlParameters: urlParameters,
                                             request: &request)
                
            case .requestParametersAndHeaders(let bodyParameters,
                                              let bodyEncoding,
                                              let urlParameters,
                                              let additionalHeaders):
                
                addAdditionalHeaders(additionalHeaders, request: &request)
                try configureParameters(bodyParameters: bodyParameters,
                                             bodyEncoding: bodyEncoding,
                                             urlParameters: urlParameters,
                                             request: &request)
            }
            return request
        } catch {
            throw error
        }
    }
    
    private func configureParameters(bodyParameters: Parameters?,
                                         bodyEncoding: ParameterEncoding,
                                         urlParameters: Parameters?,
                                         request: inout URLRequest) throws {
        do {
            try bodyEncoding.encode(urlRequest: &request,
                                    bodyParameters: bodyParameters, urlParameters: urlParameters)
        } catch {
            throw error
        }
    }
    
    private func addAdditionalHeaders(_ additionalHeaders: HTTPHeaders?, request: inout URLRequest) {
        guard let headers = additionalHeaders else { return }
        for (key, value) in headers {
            request.setValue(value, forHTTPHeaderField: key)
        }
    }
}
