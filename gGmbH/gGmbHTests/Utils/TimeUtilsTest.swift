//
//  TimeUtilsTest.swift
//  gGmbHTests
//
//  Created by Alejandro Gelos on 12/10/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import XCTest
@testable import gGmbH

class TimeUtilsTest: XCTestCase {

    private var timeUtils: TimeUtils!
    override func setUp() {
        super.setUp()
        timeUtils = TimeUtils()
    }

    override func tearDown() {
        timeUtils = nil
        super.tearDown()
    }

    func testGetFormatedTime1() {
        // given 10.000 milli sec
        let milliSec = 10000.0
        let result = timeUtils.getFormatedTime(fromMilli: milliSec)
        
        // expected result is "0:10"
        XCTAssertEqual(result, "0:10")
    }
    
    func testGetFormatedTime2() {
        // given 290.0000 milli sec
        let milliSec = 290000.0
        let result = timeUtils.getFormatedTime(fromMilli: milliSec)
        
        // expected result is "4:50"
        XCTAssertEqual(result, "4:50")
    }
    
    func testGetFormatedTime3() {
        // given 965.000 milli sec
        let milliSec = 965000.0
        let result = timeUtils.getFormatedTime(fromMilli: milliSec)
        
        // expected result is "16:05"
        XCTAssertEqual(result, "16:05")
    }
    
    func testGetFormatedTime4() {
        // given 8165.000 milli sec
        let milliSec = 8165000.0
        let result = timeUtils.getFormatedTime(fromMilli: milliSec)
        
        // expected result is "2:16:05"
        XCTAssertEqual(result, "2:16:05")
    }

    func testPerformanceExample() {
        self.measure {
            _ = timeUtils.getFormatedTime(fromMilli: 999999)
        }
    }

}
