//
//  DateUtilsTest.swift
//  gGmbHTests
//
//  Created by Alejandro Gelos on 12/10/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import XCTest
@testable import gGmbH

class DateUtilsTest: XCTestCase {

    private var dateUtils: DateUtils!
    override func setUp() {
        super.setUp()
        dateUtils = DateUtils()
    }

    override func tearDown() {
        dateUtils = nil
        super.tearDown()
    }

    func testGetDate() {
        // given a string date "1987-08-31T07:00:00Z"
        let dummyDate = dateUtils.getDate(fromString: "1987-08-31T07:00:00Z")
        
        // expected a date with description as "1987-08-31 07:00:00 +0000"
        XCTAssertEqual(dummyDate?.description, "1987-08-31 07:00:00 +0000")
    }
    
    func testGetYearFromDate() {
        // given a date with year 1987
        let dummyDate = Date(timeIntervalSince1970: 978307200.0)
        let yearString = dateUtils.getYear(fromDate: dummyDate)
        
        // expected a string with year "2001"
        XCTAssertEqual(yearString, "2001")
    }
    
    func testGetYearFromString() {
        // given a string date "1987-08-31T07:00:00Z"
        let yearString = dateUtils.getYear(fromString: "1987-08-31T07:00:00Z")
        
        // expected a string with year "1987"
        XCTAssertEqual(yearString, "1987")
    }
}
