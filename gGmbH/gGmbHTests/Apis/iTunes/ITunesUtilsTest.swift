//
//  ITunesUtilsTest.swift
//  gGmbHTests
//
//  Created by Alejandro Gelos on 12/10/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import XCTest
@testable import gGmbH

class ITunesUtilsTest: XCTestCase {

    private var iTunesUtils:ITunesUtils!
    override func setUp() {
        super.setUp()
        iTunesUtils = ITunesUtils()
    }

    override func tearDown() {
        iTunesUtils = nil
        super.tearDown()
    }
    
    func testPrepareTextForRequest() {
        // given a string with spaces
        let resultString = iTunesUtils.prepareTextForRequest(text: " Michael Jackson  b  ")
        
        // expected a trimmed and midle spaces replaced by +
        XCTAssertEqual(resultString, "Michael+Jackson++b")
    }
    
    func testIsSearchResponseValid() {
        // given a dummy urlResponse
        let dummyUrlResponse = URLResponse(url: URL(string: "https://test.com")!, mimeType: nil, expectedContentLength: 0, textEncodingName: nil)
        
        // expected test to be found in urlresponse
        XCTAssertTrue(iTunesUtils.isSearchResponseValid(searchText: "test", response: dummyUrlResponse))
    }
}
